import java.util.Scanner;

public class SimplestCalculator {
	/* the actual calculator application that performs
	 * mathematical functions using Math class
	 */
	public static void main (String[] args) {
		// Create a Math object
		Math math = new Math();
		// Create a Scanner object
		Scanner input = new Scanner(System.in);
		
		System.out.println("Welcome to Simplest Calculator!");
		// Print new lines
		String newline = System.getProperty("line.separator");
		System.out.println(newline);
		
		// Get first & second numbers from user input
		System.out.println("Enter First Number:");
		int left = input.nextInt();
		System.out.println("Enter Second Number:");
		int right = input.nextInt();
		
		// Get a math operation
		System.out.println("Enter math operation (+,-,x, or /) to perform");
		String operation = input.next();
		
		// Call corresponding method of Math object
		if (operation.equals("+"))
			System.out.println(math.add(left, right));
		else if (operation.equals("-"))
			System.out.println(math.substract(left, right));
		else if (operation.equals("x"))
			System.out.println(math.multiply(left, right));
		else if (operation.equals("/"))
			System.out.println(math.divide(left, right));
		else
			System.out.println("Invalid operation");
	}
}
