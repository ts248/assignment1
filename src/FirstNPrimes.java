import java.util.Scanner;

public class FirstNPrimes {
	/* Print out the first n orime numbers */
	public boolean isPrime (int num) {
		// Check if num is prime
		for (int i = 2; i < num; i++) {
			if (num % i == 0)
				return false;
		}
		return true;
	}
	
	public static void main(String[] args) {
		// Create FirstNPrimes & Scanner objects
		FirstNPrimes fnp = new FirstNPrimes();
		Scanner input = new Scanner(System.in);
		// Get user input
		System.out.println("Welcome!");
		System.out.println("--------");
		System.out.println("Enter the number of prime numbers to be displayed:");
		int numOfPrime = input.nextInt();
		
		int count = 0;
		int currentNum = 2;
		while (count < numOfPrime) {
			if (fnp.isPrime(currentNum)) {
				System.out.print(currentNum + " ");
				count++;
			}
			currentNum++;
		}
	}

}
