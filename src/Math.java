
public class Math {
	/* Math class performs four mathematical operations
	 * and returns results to the calling function.
	 */
	public int add (int left, int right) {
		return left + right;
	}
	
	public int substract (int left, int right) {
		return left - right;
	}
	
	public int multiply (int left, int right) {
		return left * right;
	}
	
	public int divide (int left, int right) {
		return left / right;
	}
}
